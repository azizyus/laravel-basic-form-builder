<?php


namespace Azizyus\FormBuilder\Models;


use Illuminate\Http\Request;

trait RelationTrait
{

    public function forms()
    {
        return $this->belongsToMany(Form::class,'form_model_relations','modelId','formId')
            ->where('modelEnum',get_class());
    }

    public function mappedForms()
    {
        return $this->forms->keyBy('key')->map(function($form){
            return [
                'form' => $form->build(),
                'title' => $form->title,
                'inputs' => $form->mapInputs()
            ];
        });
    }

    public function syncForms(Request $request,$key='forms')
    {
        FormModelRelation::where('modelEnum',get_class())->delete();
        foreach ($request->get($key,[]) as $f)
        {
            FormModelRelation::create([
                'modelId' => $this->id,
                'formId' => $f,
                'modelEnum' => get_class()
            ]);
        }
    }

}
