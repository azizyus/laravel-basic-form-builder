<?php


namespace Azizyus\FormBuilder\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class FormInput extends Model
{

    protected $table = "form_inputs";

    protected $fillable = [

        'label',
        'value',
        'type',
        'name',
        'slug',
        'sort',
        'multiple',
        'expanded',
        'isRequired',
        'translateFlag',
        'class',
        'wrapperClass',
        'placeHolder',
        'formId'

    ];

    public function setNameAttribute($value)
    {
        $this->attributes["name"] = $value;
        $this->attributes["slug"] = Str::slug($value);
    }

    public function mapValues()
    {
        return $this->values->sortBy('sort')->keyBy('value')->map(function($value){
            return $value->value;
        });
    }

    public function values()
    {
        return $this->hasMany(FormInputValue::class,"inputId","id");
    }

}
