<?php


namespace Azizyus\FormBuilder\Models;


use Azizyus\FormBuilder\Builder\Builder;
use Illuminate\Database\Eloquent\Model;

class Form extends Model
{

    protected $table = "forms";


    protected $fillable = [
        'id',
        'title',
        'key',
    ];

    public function getFormInputsSorted()
    {
        return $this->formInputs->sortBy('sort')->all();
    }


    public function formInputs()
    {
        return $this->hasMany(FormInput::class,"formId","id");
    }

    public function build()
    {
        $b = new Builder();
        return $b->build($this);
    }

    public function mapInputs()
    {
        return $this->formInputs->sortBy('sort')->map(function($input){

            return [
                'label' => $input->label,
                'type' => $input->type,
                'value' => $input->value,
                'values' => $input->mapValues()
            ];

        });
    }

}
