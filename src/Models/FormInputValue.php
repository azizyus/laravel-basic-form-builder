<?php


namespace Azizyus\FormBuilder\Models;


use Illuminate\Database\Eloquent\Model;

class FormInputValue extends Model
{

    protected $table = 'form_input_values';

    protected $fillable = [
        'value',
        'inputId',
        'sort',
        'isDefault',
    ];

}
