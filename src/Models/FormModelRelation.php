<?php


namespace Azizyus\FormBuilder\Models;


use Illuminate\Database\Eloquent\Model;

class FormModelRelation extends Model
{


    protected $table = 'form_model_relations';

    protected $fillable = [
        'formId',
        'modelId',
        'modelEnum',
    ];

}
