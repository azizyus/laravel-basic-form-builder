<?php


namespace Azizyus\FormBuilder\Builder;


use Azizyus\FormBuilder\Forms\GenericForm;
use Azizyus\FormBuilder\Models\Form;

class Builder
{

    public function build(Form $form)
    {
        return app('laravel-form-builder')->create(GenericForm::class,[
            'form' => $form,
            'method' => 'POST',
            'url' => config('formBuilder.mailPostUrl'),
        ]);
    }


}
