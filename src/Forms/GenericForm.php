<?php


namespace Azizyus\FormBuilder\Forms;


use Azizyus\FormBuilder\Models\Form as DBForm;
use Illuminate\Support\Str;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class GenericForm extends Form
{




    public function buildForm()
    {
        /**
         * @var \Azizyus\FormBuilder\Models\Form $form
         */
        $form = $this->getFormOption('form');
        foreach ($form->getFormInputsSorted() as $input)
        {
            $label = call_user_func(function($input){
                if($input->translateFlag)
                    return trans($input->label);
                return $input->label;
            },$input);

            $this->add($input->slug,$input->type,[

                'label' => $label,
                'attr' => [
                    'class' => call_user_func(function() use($input){
                        if(!$input->class) return 'form-control';
                        return "form-control $input->class";
                    }),
                    'required' => call_user_func(function()use($input){
                        if($input->isRequired)
                            return true;
                        return false;
                    }),
                    'placeholder' => call_user_func(function()use($input,$label){
                        if($input->placeHolder)
                        {
                            if($input->translateForm)
                                return trans($input->placeHolder);
                            return $input->placeHolder;
                        }
                        return $label;
                    })
                ],
                'wrapper' => [
                    'class' =>
                        call_user_func(function() use($input){
                            if(!$input->wrapperClass) return 'form-group';
                            return  "form-group $input->wrapperClass";
                        })
                ],

                'choices' => $input->mapValues()->toArray(),
                'selected' => call_user_func(function()use($input){
                    $d = $input->values()->where('isDefault',true)->first();
                    if($d)
                        return $d->value;
                    return null;
                }),
                'value' => $input->value,
                'expanded' => $input->expanded,
                'multiple' => $input->multiple,

            ]);
        }

        $this->add('formId',Field::HIDDEN,[
            'value' => $form->id
        ]);
//
    }

}
