<?php


namespace Azizyus\FormBuilder\Forms;


use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class FormInputValueCrudForm extends Form
{

    public function buildForm()
    {

        $this
            ->add('value',Field::TEXT)
            ->add('isDefault',Field::CHECKBOX)
            ->add('sort',Field::NUMBER,[
            ])
        ->add('Save',Field::BUTTON_SUBMIT);

    }

}
