<?php


namespace Azizyus\FormBuilder\Forms;


use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class FormCrudForm extends Form
{

    public function buildForm()
    {
        $this
            ->add('title',Field::TEXT,[
                'attr' => [
                    'required' => 'required'
                ]
            ])
            ->add('key',Field::TEXT,[
                'attr' => [
                    'required' => 'required'
                ]
            ])
            ->add('Save',Field::BUTTON_SUBMIT);
    }

}
