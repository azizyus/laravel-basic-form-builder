<?php


namespace Azizyus\FormBuilder\Forms\Fields;


use Kris\LaravelFormBuilder\Fields\FormField;

class TextRepeat extends FormField
{

    protected function getTemplate()
    {
        return 'FormBuilder::repeat_text';
    }

}
