<?php


namespace Azizyus\FormBuilder\Forms;


use App\Forms\Repeater\Repeater;
use Azizyus\FormBuilder\Forms\Fields\TextRepeat;
use Kris\LaravelFormBuilder\Field;
use Kris\LaravelFormBuilder\Form;

class FormInputCrudForm extends Form
{

    protected function preventDuplicate($name)
    {

    }

    public function buildForm()
    {

        $this->addCustomField('repeat_text',TextRepeat::class);

        $this->add('name',Field::TEXT);
        $this->add('label',Field::TEXT);

        $this->add('expanded',Field::SELECT,[

            'choices' => [
                0 => 'No',
                1 => 'Yes'
            ]

        ]);
        $this->add('isRequired',Field::SELECT,[

            'choices' => [
                0 => 'No',
                1 => 'Yes'
            ]

        ]);
        $this->add('multiple',Field::SELECT,[

            'choices' => [
                0 => 'No',
                1 => 'Yes'
            ]

        ]);

        $this->add('placeHolder',Field::TEXT);

        $this->add('placeHolder',Field::TEXT);

        $this->add('sort',Field::NUMBER);
        $this->add('value',Field::TEXT,[
                'attr' => [
                    'class' => 'value form-control',

                ],
                'label' => 'Value',
                'repeaterClass' => 'value'
        ]);

        $this->add('class',Field::TEXT,[]);
        $this->add('wrapperClass',Field::TEXT,[]);


        $this->add('translateFlag',Field::SELECT,[

                'choices' => [
                    0 => 'No',
                    1 => 'Yes'
                ]

        ])->add('type',Field::SELECT,[

                'choices' => [

                    Field::TEXT => "Text",
                    Field::TEXTAREA => "Textarea",
                    Field::SELECT => "Select",
                    Field::NUMBER => "Number",
                    Field::EMAIL => "Email",
                    Field::DATE => "Date",
                    Field::CHOICE => "Choice (Multiple checkbox or radio button)",
                    Field::BUTTON_SUBMIT => "Submit Button",

                ]

            ])
            ->add('Save',Field::BUTTON_SUBMIT);

    }

}
