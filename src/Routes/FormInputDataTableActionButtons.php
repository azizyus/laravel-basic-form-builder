<?php


namespace Azizyus\FormBuilder\Routes;


use App\DataTables\ActionButton;
use Azizyus\FormBuilder\ExternalStructureDefinitions\IIndexDataTableActionButtons;

class FormInputDataTableActionButtons implements IIndexDataTableActionButtons
{
    public function build() : \Closure
    {
        return ActionButton::editDeleteButtonPack("formInputs.edit","formInputs.destroy");
    }
}
