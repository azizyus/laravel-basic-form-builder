<?php


namespace Azizyus\FormBuilder\Routes;


use Azizyus\FormBuilder\ExternalStructureDefinitions\IFormInputValueCrudRoutes;

class FormInputValuesCrudRoutes implements IFormInputValueCrudRoutes
{
    public function update(Int $id): String
    {
        return route('formValues.update',[$id]);
    }


    public function store(Int $inputId): String
    {
        return route('formValues.store',['inputId'=>$inputId]);
    }

    public function destroy(Int $id): String
    {
        return route('formValues.destroy',$id);
    }


}
