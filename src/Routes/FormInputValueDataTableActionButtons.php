<?php


namespace Azizyus\FormBuilder\Routes;


use App\DataTables\ActionButton;
use Azizyus\FormBuilder\ExternalStructureDefinitions\IIndexDataTableActionButtons;

class FormInputValueDataTableActionButtons implements IIndexDataTableActionButtons
{
    public function build() : \Closure
    {
        return ActionButton::editDeleteButtonPack("formValues.edit","formValues.destroy");
    }
}
