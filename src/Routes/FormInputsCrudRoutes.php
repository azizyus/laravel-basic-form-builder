<?php


namespace Azizyus\FormBuilder\Routes;


use Azizyus\FormBuilder\ExternalStructureDefinitions\IFormInputCrudRoutes;

class FormInputsCrudRoutes implements IFormInputCrudRoutes
{
    public function update(Int $id): String
    {
        return route('formInputs.update',[$id]);
    }


    public function store(Int $formId): String
    {
        return route('formInputs.store',['formId'=>$formId]);
    }

    public function destroy(Int $id): String
    {
        return route('formInputs.destroy',$id);
    }


}
