<?php


namespace Azizyus\FormBuilder\Routes;


use Illuminate\Support\Facades\Route;

class Routes
{


    public static function formBuilderRoutes()
    {

       Route::group(['namespace'=>'\\Azizyus\\FormBuilder\\Controllers'],function(){
           Route::resource("forms","FormBuilderController");
           Route::resource("formInputs","FormInputController");
           Route::resource("formValues","FormValueController");
       });

    }

    public static function mailRoutes()
    {
        Route::group(['namespace'=>'\\Azizyus\\FormBuilder\\Controllers'],function() {
            Route::post('mailPostUrl', 'MailController@mail')->name('mail.post');
        });
    }

}
