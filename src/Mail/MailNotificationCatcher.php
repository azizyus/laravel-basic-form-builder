<?php


namespace Azizyus\FormBuilder\Mail;


use Azizyus\FormBuilder\Notifications\MailNotification;
use Azizyus\FormBuilder\Notifications\Models\MailableData;
use Azizyus\FormBuilder\Repositories\FormRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Notification;
use Kris\LaravelFormBuilder\Field;

class MailNotificationCatcher
{

    protected $formRepository;
    public function __construct()
    {
        $this->formRepository = new FormRepository();
    }

    public function catch(Request $request)
    {

        $formId = $request->get('formId');
        $form = $this->formRepository->first($formId);
        if($form)
        {
            $slugs = $form->formInputs->pluck("slug")->toArray();
            $data = $request->only($slugs);
            $mailableData = $form->formInputs
                ->where('type','!=',Field::BUTTON_SUBMIT)
                ->map(function($input)use($data){
                return new MailableData($input->name,call_user_func(function()use($input,$data){
                    $value = $data[$input->slug]??null;
                    if(is_array($value))
                        return implode(',',$value);
                    return $value;
                }));
            });
            Notification::route('mail',config('formBuilder.notificationTargetMail'))->notify(new MailNotification($mailableData));

        }
        else
        {
            throw new \Exception('cant find your form');
        }

    }

}
