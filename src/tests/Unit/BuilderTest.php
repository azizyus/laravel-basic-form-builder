<?php


namespace Azizyus\FormBuilder\tests\Unit;


use Azizyus\FormBuilder\Builder\Builder;
use Azizyus\FormBuilder\Models\Form;
use Azizyus\FormBuilder\Models\FormInput;
use Azizyus\FormBuilder\tests\BaseTest;
use Kris\LaravelFormBuilder\Field;
use Mockery\Mock;
use Kris\LaravelFormBuilder;

class BuilderTest extends BaseTest
{


    public function testModelToForm()
    {
        /**
         * @var Mock $formMocked
         */


        /**
         * @var Mock $formInputMocked
         */

        /**
         * @var LaravelFormBuilder\Form $expectedForm
         * @var LaravelFormBuilder\Form $buildedForm
         */

        $formMocked = \Mockery::mock( Form::make([
                    'id' => '1',
                    'title'=>'test_name'
                ]))->makePartial();


        $formInputMocked = \Mockery::mock(  FormInput::make([

            'label' => 'test_name',
            'value' => '2',
            'type'  => 'text',
            'isRequired'  => true,
            'slug'  => 'test_name',

        ]))->makePartial();

        $formInputMocked->shouldReceive('mapValues')->andReturn(collect());
        $formInputMocked->shouldReceive('values')->andReturn(collect());
        $formMocked->shouldReceive('getFormInputsSorted')->andReturn([$formInputMocked]);

        $builder = new Builder();
        $buildedForm = $builder->build($formMocked);
        $formBuilder = $this->app['laravel-form-builder'];
            $expectedForm = $formBuilder->plain([
                'method' => 'POST'
            ])
            ->add('test_name', 'text', [
                'label' => 'test_name',
                'attr' => [
                    'required' => true,
                    'placeholder' => 'test_name',
                ],
                'value' => '2'
            ])
            ->add('formId', 'hidden', [
                    'value' => 1
            ]);

        $this->assertEquals(form($expectedForm),form($buildedForm));

    }

}
