<?php


namespace Azizyus\FormBuilder\tests\Unit;


use Azizyus\FormBuilder\Routes\Routes;
use Azizyus\FormBuilder\tests\BaseTest;

class RouteNamespaceTest extends BaseTest
{

    //basic namespace check
    public function testGlobalNamesSpacesForControllers()
    {
        Routes::formBuilderRoutes();
        $this->assertEquals(action('Azizyus\FormBuilder\Controllers\FormBuilderController@index'),route('forms.index'));
    }

}
