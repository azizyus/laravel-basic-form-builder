<?php


namespace Azizyus\FormBuilder\tests;


use Azizyus\LaravelLanguageHelper\App\LanguageHelperServiceProvider;
use Azizyus\Payment\PaymentServiceProvider;
use CurrencyManager\CurrencyManagerServiceProvider;
use Kris\LaravelFormBuilder\FormBuilderServiceProvider;
use Orchestra\Testbench\TestCase;

class BaseTest extends TestCase
{

    protected function setUp(): void
    {
        parent::setUp();

    }

    protected function getPackageProviders($app)
    {
        return [
            FormBuilderServiceProvider::class
        ];
    }

}
