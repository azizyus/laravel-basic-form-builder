<?php


namespace Azizyus\FormBuilder\ExternalStructureDefinitions;


interface IFormInputCrudRoutes
{

    public function update(Int $id) : String;

    public function store(Int $formId) : String;

    public function destroy(Int $id) : String;

}
