<?php


namespace Azizyus\FormBuilder\ExternalStructureDefinitions;


interface IIndexDataTableActionButtons
{

    public function build() : \Closure;

}
