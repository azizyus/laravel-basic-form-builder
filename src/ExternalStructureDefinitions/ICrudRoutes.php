<?php


namespace Azizyus\FormBuilder\ExternalStructureDefinitions;


interface ICrudRoutes
{

    public function update(Int $id) : String;

    public function store() : String;

    public function destroy(Int $id) : String;

}
