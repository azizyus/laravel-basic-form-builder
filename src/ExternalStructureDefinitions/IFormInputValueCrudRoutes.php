<?php


namespace Azizyus\FormBuilder\ExternalStructureDefinitions;


interface IFormInputValueCrudRoutes
{

    public function update(Int $id) : String;

    public function store(Int $inputId) : String;

    public function destroy(Int $id) : String;

}
