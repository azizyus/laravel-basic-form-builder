<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormInputsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form_inputs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->boolean('multiple')->nullable();
            $table->boolean('expanded')->nullable();
            $table->boolean('isRequired')->nullable();
            $table->string('slug')->nullable();
            $table->string('label')->nullable();
            $table->string('placeHolder')->nullable();
            $table->string('type')->nullable();
            $table->string('class')->nullable();
            $table->string('wrapperClass')->nullable();
            $table->text('value')->nullable();
            $table->unsignedInteger('formId');
            $table->unsignedInteger('sort')->nullable();
            $table->boolean('translateFlag')->default(false);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('form_inputs');
    }
}
