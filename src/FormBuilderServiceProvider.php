<?php


namespace Azizyus\FormBuilder;


use Azizyus\FormBuilder\Forms\Fields\TextRepeat;
use Illuminate\Support\ServiceProvider;

class FormBuilderServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->loadMigrationsFrom(__DIR__.'/Migrations');
        $this->loadViewsFrom(__DIR__.'/Html','FormBuilder');

        $this->mergeConfigFrom(__DIR__.'/Config/config.php','formBuilder');


        config()->set('laravel-form-builder.defaults.wrapper_class','form-group col-md-12');
        config()->set('laravel-form-builder.form','FormBuilder::form');

        config()->set('formBuilder.successSessionKey','actionStatus');

    }


}
