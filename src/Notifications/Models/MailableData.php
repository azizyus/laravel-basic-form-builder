<?php


namespace Azizyus\FormBuilder\Notifications\Models;


class MailableData
{


    public $name;
    public $value;

    public function __construct($name,$value)
    {
         $this->name = $name;
         $this->value = $value;
    }


}
