<?php if ($showLabel && $showField): ?>

<div class="{{$options['repeaterClass']}}">
    <div data-repeater-list="{{$options['repeaterClass']}}">
        <?php if ($showLabel && $options['label'] !== false && $options['label_show']): ?>
    <?= Form::customLabel($name, $options['label'], $options['label_attr']) ?>
<?php endif; ?>
        <div data-repeater-item class="mb-1">
<?php if ($options['wrapper'] !== false): ?>
<div <?= $options['wrapperAttrs'] ?> >
    <?php endif; ?>
    <?php endif; ?>
        <div class="input-group">


    <?php if ($showField): ?>
    <?= Form::input($type, $name, $options['value'], $options['attr']) ?>
        <input class="btn btn-md btn-primary pull-right" data-repeater-delete type="button" value="Delete"/>

<?php endif; ?>
        </div>

    <?php if ($showLabel && $showField): ?>
    <?php if ($options['wrapper'] !== false): ?>
</div>
        </div>
    </div>
    <input class="btn btn-md btn-primary" style="width: 100%;" data-repeater-create type="button" value="Add"/>

</div>
<?php endif; ?>
<?php endif; ?>
