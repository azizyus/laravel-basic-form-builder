<?php


namespace Azizyus\FormBuilder\Repositories;


use App\Forms\Repeater\Repeater;
use Azizyus\FormBuilder\Models\FormInput;
use Illuminate\Http\Request;

class FormInputRepository extends AbstractRepository
{

    public function model(): String
    {
        return FormInput::class;
    }

    public function updateOrInsert(Request $request, $id = null)
    {

        $first = $this->firstOrNew([],$id);
        $first->fill($request->only($first->getFillable()));
        if(!$first->id)
        {
            $lastInput = $this->baseQuery()->where('formId',$request->get('formId'))->orderBy('id','DESC')->first();
            $sort = $lastInput!=null ? $lastInput->sort+1 : null;
            $first->sort = $sort;
        }
        $first->save();
    }

}
