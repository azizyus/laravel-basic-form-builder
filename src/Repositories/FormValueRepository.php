<?php


namespace Azizyus\FormBuilder\Repositories;


use Azizyus\FormBuilder\Models\FormInputValue;
use Illuminate\Http\Request;

class FormValueRepository extends AbstractRepository
{

    public function model(): String
    {
        return FormInputValue::class;
    }

    public function updateOrInsert(Request $request, $id = null)
    {
        $first = $this->firstOrNew([],$id);
        $first->fill($request->only($first->getFillable()));
        if(!$first->id)
        {
            $lastInput = $this->baseQuery()->where('inputId',$request->get('inputId'))->orderBy('id','DESC')->first();
            $sort = $lastInput!=null ? $lastInput->sort+1 : null;
            $first->sort = $sort;
        }
        $first->save();
    }


}
