<?php


namespace Azizyus\FormBuilder\Repositories;


use Azizyus\FormBuilder\Models\Form;
use Illuminate\Http\Request;

class FormRepository extends AbstractRepository
{


    public function model(): String
    {
        return Form::class;
    }

    public function updateOrInsert(Request $request, $id = null)
    {
        $found = $this->firstOrNew([],$id);
        $found->fill($request->only($found->getFillable()));
        $found->save();
    }

}
