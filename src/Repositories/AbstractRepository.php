<?php


namespace Azizyus\FormBuilder\Repositories;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Scope;
use Illuminate\Http\Request;

/**
 * Class AbstractRepository
 * @package Azizyus\Payment\Repository
 */
abstract class AbstractRepository
{

    /**
     * @return String
     *
     * return namespace of model to make this class more generic
     */
    abstract public function model() : String;

    /**
     * @return mixed
     */
    protected function newModel()
    {
        $n = $this->model();
        return (new $n);
    }

    /**
     * @return Builder
     */
    public function baseQuery() : Builder
    {
        /**
         * @var Builder $builder
         */
        $builder = $this->model()::query();
        foreach ($this->scopes as $s)
        {
            $class = get_class($s);
            if(!in_array($class,$this->tempWithoutScope))
                $builder->withGlobalScope($class,$s);
        }
        $this->tempWithoutScope = [];
        return $builder;
    }

    /**
     * @var array
     */
    protected $scopes=[];

    /**
     * @param Scope $scope
     * @return AbstractRepository
     */
    public function pushScope(Scope $scope)
    {
        $this->scopes[] = $scope;
        return $this;
    }

    /**
     * @param $scope
     * @return AbstractRepository
     */
    protected $tempWithoutScope = [];
    public function withoutScope($removeScope)
    {
        $this->tempWithoutScope[] = $removeScope;
        return $this;
    }

    public function getScopes()
    {
        return $this->scopes;
    }

    /**
     * @param Request $request
     * @param null $id
     * @return mixed
     */
    abstract public function updateOrInsert(Request $request, $id=null);

    /**
     * @param null $id
     * @return Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|null
     */
    public function first($id=null)
    {
        if(!$id) return null;
        return $this->baseQuery()->find($id);
    }

    /**
     * @return Builder|\Illuminate\Database\Eloquent\Model|object|null
     */
    public function runFirst()
    {
        return $this->baseQuery()->first();
    }

    /**
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function runGet()
    {
        return $this->baseQuery()->get();
    }

    /**
     * @param \Closure $c
     * @param null $id
     * @return Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed|null
     */
    protected function firstOrNewLazy(\Closure $c, $id =null)
    {
        $found = $this->first($id);
        if(!$found)
        {
            $found =  $this->newModel();
            $found->fill(call_user_func($c));
        }
        return $found;
    }

    /**
     * @param array $defaultColumns
     * @param null $id
     * @return Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|\Illuminate\Database\Eloquent\Model|mixed|null
     */
    protected function firstOrNew($defaultColumns=[], $id=null)
    {
        $found = $this->first($id);
        if(!$found)
        {
            $found = $this->newModel();
            $found->fill($defaultColumns);
        }
        return $found;
    }

    /**
     * @return Builder[]|\Illuminate\Database\Eloquent\Collection
     */
    public function all()
    {
        return $this->baseQuery()->get();
    }

    /**
     * @param $id
     * @throws \Exception
     */
    public function delete($id)
    {
        //we need the model its self to run delete events while ->delete();, mass deleting would be bad
        $found = $this->first($id);
        if($found) $found->delete();
//         return $this->baseQuery()->where('id',$id)->delete();
    }

}
