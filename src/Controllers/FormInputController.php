<?php


namespace Azizyus\FormBuilder\Controllers;


use App\Forms\BuilderImplementations\CrudRoutes;
use App\Forms\BuilderImplementations\DataTableActionButtons;
use Azizyus\FormBuilder\Routes\FormInputDataTableActionButtons;
use Azizyus\FormBuilder\Routes\FormInputsCrudRoutes;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Panel\PanelController;
use Azizyus\FormBuilder\Controllers\FormInputsManager;
use Azizyus\FormBuilder\Controllers\FormManager;
use Azizyus\FormBuilder\DataTable\FormInputsDataTable;
use Azizyus\FormBuilder\DataTable\FormsDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Kris\LaravelFormBuilder\FormBuilder;

class FormInputController extends Controller
{

    protected $formInputsManager;
    public function __construct()
    {
        $this->formInputsManager = new FormInputsManager();
        $this->formInputsManager->setCrudUrlRoutes(new FormInputsCrudRoutes());
    }

    public function index(FormInputsDataTable $table,Request $request)
    {
        $table->setActionButtons(new FormInputDataTableActionButtons());
        $table->setFormId($this->formInputsManager->getFormIdFromRequest($request));
        $table->setValuesRouteName('formValues.index');
        return $table->render(config('formBuilder.dataTableViewName'),[
            'title' => 'Form Inputs  ['.$this->formInputsManager->getFormTitle($request).']',
            'buttons' => [
                [
                    'title' => 'Add Form Input',
                    'route' => route('formInputs.create',['formId'=>$this->formInputsManager->getFormIdFromRequest($request)])
                ]
            ]
        ]);
    }

    public function create(FormBuilder $formBuilder,Request $request)
    {
        return view(config('formBuilder.crudViewName'))->with($this->formInputsManager->create($formBuilder,$request));
    }

    public function store(Request $request)
    {
        $this->formInputsManager->store($request);
        return Redirect::route('formInputs.index',['formId'=>$this->formInputsManager->getFormIdFromRequest($request)]);
    }

    public function edit(FormBuilder $formBuilder,Request $request,$id)
    {
        return view(config('formBuilder.crudViewName'))->with($this->formInputsManager->edit($formBuilder,$request,$id));
    }

    public function update(Request $request,$id)
    {
        $this->formInputsManager->update($request,$id);
        return Redirect::route('formInputs.index',['formId'=>$this->formInputsManager->getFormInputParentId($id)]);
    }

    public function destroy($id)
    {
        $found = $this->formInputsManager->getRepository()->first($id);
        $this->formInputsManager->destroy($id);
        return Redirect::route('formInputs.index',['formId'=>$found->formId]);
    }

}
