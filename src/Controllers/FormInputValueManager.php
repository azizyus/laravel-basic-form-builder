<?php


namespace Azizyus\FormBuilder\Controllers;


use App\Forms\Repeater\Repeater;
use Azizyus\FormBuilder\ExternalStructureDefinitions\IFormInputCrudRoutes;
use Azizyus\FormBuilder\ExternalStructureDefinitions\IFormInputValueCrudRoutes;
use Azizyus\FormBuilder\Forms\FormCrudForm;
use Azizyus\FormBuilder\Forms\FormInputCrudForm;
use Azizyus\FormBuilder\Forms\FormInputValueCrudForm;
use Azizyus\FormBuilder\Repositories\FormInputRepository;
use Azizyus\FormBuilder\Repositories\FormRepository;
use Azizyus\FormBuilder\Repositories\FormValueRepository;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;

class FormInputValueManager
{

    /**
     * @var IFormInputCrudRoutes
     */
    protected $crudUrlRoutes;


    /**
     * @var FormInputRepository
     */
    protected $repository;
    public function __construct()
    {
        $this->repository = new FormValueRepository();
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function getInputTitle(Request $request)
    {
        return (new FormInputRepository())->first($this->getInputIdFromRequest($request))->label;
    }

    public function getInputIdFromRequest(Request $request)
    {
        return $request->get('inputId');
    }

    public function getParentInputFormId(Request $request)
    {
        return (new FormInputRepository())->baseQuery()->where('id',$this->getInputIdFromRequest($request))->first()->formId;
    }

    public function getFormValueParentId($id)
    {
        return $this->repository->first($id)->inputId;
    }

    public function setCrudUrlRoutes(IFormInputValueCrudRoutes $c)
    {
        $this->crudUrlRoutes = $c;
    }

    public function create(FormBuilder $formBuilder,Request $request)
    {
        return [
            'forms' => [
                'form' => $formBuilder->create(FormInputValueCrudForm::class,[
                    'method' => 'POST',
                    'url' => $this->crudUrlRoutes->store($this->getInputIdFromRequest($request))
                ])
            ],

        ];
    }

    public function store(Request $request)
    {
        $this->repository->updateOrInsert($request);
    }

    public function edit(FormBuilder $formBuilder,Request $request,$id)
    {
        return [
            'forms' => [
                'form' => $formBuilder->create(FormInputValueCrudForm::class,[
                    'method' => 'PUT',
                    'url' => $this->crudUrlRoutes->update($id),
                    'model' => $this->repository->first($id)
                ])
            ],

        ];
    }

    public function update(Request $request,$id)
    {
        $this->repository->updateOrInsert($request,$id);
    }

    public function destroy($id)
    {
        $this->repository->delete($id);
    }


}
