<?php


namespace Azizyus\FormBuilder\Controllers;


use Azizyus\FormBuilder\Routes\FormInputDataTableActionButtons;
use Azizyus\FormBuilder\Routes\FormInputValueDataTableActionButtons;
use Azizyus\FormBuilder\Routes\FormInputValuesCrudRoutes;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Panel\PanelController;
use Azizyus\FormBuilder\DataTable\FormInputValuesDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Kris\LaravelFormBuilder\FormBuilder;

class FormValueController extends Controller
{

    protected $formInputValueManager;
    public function __construct()
    {
        $this->formInputValueManager = new FormInputValueManager();
        $this->formInputValueManager->setCrudUrlRoutes(new FormInputValuesCrudRoutes());
    }

    public function index(FormInputValuesDataTable $table,Request $request)
    {
        $table->setActionButtons(new FormInputValueDataTableActionButtons());
        $table->setInputId($this->formInputValueManager->getInputIdFromRequest($request));
        return $table->render(config('formBuilder.dataTableViewName'),[
            'title' => 'Form Values  ['.$this->formInputValueManager->getInputTitle($request).']',
            'buttons' => [
                [
                    'title' => 'Go to parent form inputs',
                    'route' => route('formInputs.index',['formId'=>$this->formInputValueManager->getParentInputFormId($request)])
                ],
                [
                    'title' => 'Add Input Value',
                    'route' => route('formValues.create',['inputId'=>$this->formInputValueManager->getInputIdFromRequest($request)])
                ]
            ]
        ]);
    }

    public function create(FormBuilder $formBuilder,Request $request)
    {
        return view(config('formBuilder.crudViewName'))->with($this->formInputValueManager->create($formBuilder,$request));
    }

    public function store(Request $request)
    {
        $this->formInputValueManager->store($request);
        return Redirect::route('formValues.index',['inputId'=>$this->formInputValueManager->getInputIdFromRequest($request)]);
    }

    public function edit(FormBuilder $formBuilder,Request $request,$id)
    {
        return view(config('formBuilder.crudViewName'))->with($this->formInputValueManager->edit($formBuilder,$request,$id));
    }

    public function update(Request $request,$id)
    {
        $this->formInputValueManager->update($request,$id);
        return Redirect::route('formValues.index',['inputId'=>$this->formInputValueManager->getFormValueParentId($id)]);
    }

    public function destroy($id)
    {
        $found = $this->formInputValueManager->getRepository()->first($id);
        $this->formInputValueManager->destroy($id);
        return Redirect::route('formValues.index',['inputId'=>$found->inputId]);
    }

}
