<?php


namespace Azizyus\FormBuilder\Controllers;


use Azizyus\FormBuilder\Mail\MailNotificationCatcher;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class MailController extends Controller
{

    public function mail(Request $request)
    {
        (new MailManager())->mail($request);
        return back()->with(config('formBuilder.successSessionKey'),true);
    }

}
