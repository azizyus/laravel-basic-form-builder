<?php


namespace Azizyus\FormBuilder\Controllers;


use App\Forms\BuilderImplementations\CrudRoutes;
use App\Forms\BuilderImplementations\DataTableActionButtons;
use Illuminate\Routing\Controller;
use App\Http\Controllers\Panel\PanelController;
use Azizyus\FormBuilder\Controllers\FormManager;
use Azizyus\FormBuilder\DataTable\FormsDataTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Route;
use Kris\LaravelFormBuilder\FormBuilder;

class FormBuilderController extends Controller
{

    protected $formBuilderManager;
    public function __construct()
    {
        $this->formBuilderManager = new FormManager();
        $this->formBuilderManager->setCrudUrlRoutes(new CrudRoutes());
    }

    public function index(FormsDataTable $table)
    {
        $table->setActionButtons(new DataTableActionButtons());
        $table->setInputRouteName('formInputs.index');
        return $table->render(config('formBuilder.dataTableViewName'),[
            'title' => 'Forms',
            'buttons' => [
                [
                    'title' => 'Add Form',
                    'route' => route('forms.create')
                ]
            ]
        ]);
    }

    public function create(FormBuilder $formBuilder)
    {
        return view(config('formBuilder.crudViewName'))->with($this->formBuilderManager->create($formBuilder));
    }

    public function store(Request $request)
    {
        $this->formBuilderManager->store($request);
        return Redirect::route('forms.index');
    }

    public function edit(FormBuilder $formBuilder,Request $request,$id)
    {
        return  view(config('formBuilder.crudViewName'))->with($this->formBuilderManager->edit($formBuilder,$request,$id));
    }

    public function update(Request $request,$id)
    {
        $this->formBuilderManager->update($request,$id);
        return Redirect::route('forms.index');
    }

    public function destroy($id)
    {
        $this->formBuilderManager->destroy($id);
        return Redirect::route('forms.index');
    }

}
