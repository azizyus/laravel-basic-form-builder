<?php


namespace Azizyus\FormBuilder\Controllers;


use App\Forms\Repeater\Repeater;
use Azizyus\FormBuilder\ExternalStructureDefinitions\IFormInputCrudRoutes;
use Azizyus\FormBuilder\Forms\FormCrudForm;
use Azizyus\FormBuilder\Forms\FormInputCrudForm;
use Azizyus\FormBuilder\Repositories\FormInputRepository;
use Azizyus\FormBuilder\Repositories\FormRepository;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;

class FormInputsManager
{

    /**
     * @var IFormInputCrudRoutes
     */
    protected $crudUrlRoutes;


    /**
     * @var FormInputRepository
     */
    protected $repository;
    public function __construct()
    {
        $this->repository = new FormInputRepository();
    }

    public function getRepository()
    {
        return $this->repository;
    }

    public function getFormTitle(Request $request)
    {
        return (new FormRepository())->first($this->getFormIdFromRequest($request))->title;
    }

    public function getFormIdFromRequest(Request $request)
    {
        return $request->get('formId');
    }

    public function getFormInputParentId($id)
    {
        return $this->repository->first($id)->formId;
    }

    public function setCrudUrlRoutes(IFormInputCrudRoutes $c)
    {
        $this->crudUrlRoutes = $c;
    }

    public function create(FormBuilder $formBuilder,Request $request)
    {
        return [
            'forms' => [
                'form' => $formBuilder->create(FormInputCrudForm::class,[
                    'method' => 'POST',
                    'url' => $this->crudUrlRoutes->store($this->getFormIdFromRequest($request))
                ])
            ],

        ];
    }

    public function store(Request $request)
    {
        $this->repository->updateOrInsert($request);
    }

    public function edit(FormBuilder $formBuilder,Request $request,$id)
    {
        return [
            'forms' => [
                'form' => $formBuilder->create(FormInputCrudForm::class,[
                    'method' => 'PUT',
                    'url' => $this->crudUrlRoutes->update($id),
                    'model' => $this->repository->first($id)
                ])
            ],

        ];
    }

    public function update(Request $request,$id)
    {
        $this->repository->updateOrInsert($request,$id);
    }

    public function destroy($id)
    {
        $this->repository->delete($id);
    }


}
