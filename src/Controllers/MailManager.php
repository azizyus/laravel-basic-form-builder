<?php


namespace Azizyus\FormBuilder\Controllers;


use Azizyus\FormBuilder\Mail\MailNotificationCatcher;
use Illuminate\Http\Request;

class MailManager
{

    public function mail(Request $request)
    {
        $m = new MailNotificationCatcher();
        $m->catch($request);
    }

}
