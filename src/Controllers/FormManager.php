<?php


namespace Azizyus\FormBuilder\Controllers;


use Azizyus\FormBuilder\ExternalStructureDefinitions\ICrudRoutes;
use Azizyus\FormBuilder\Forms\FormCrudForm;
use Azizyus\FormBuilder\Repositories\FormRepository;
use Illuminate\Http\Request;
use Kris\LaravelFormBuilder\FormBuilder;

class FormManager
{

    protected $formRepository;

    /**
     * @var ICrudRoutes
     */
    protected $crudUrlRoutes;
    public function __construct()
    {
        $this->formRepository = new FormRepository();
    }

    public function setCrudUrlRoutes(ICrudRoutes $c)
    {
        $this->crudUrlRoutes = $c;
    }

    public function create(FormBuilder $formBuilder)
    {
        return [
          'forms' => [
              'form' => $formBuilder->create(FormCrudForm::class,[
                  'method' => 'POST',
                  'url' => $this->crudUrlRoutes->store()
              ])
          ],

        ];
    }

    public function store(Request $request)
    {
        $this->formRepository->updateOrInsert($request);
    }

    public function edit(FormBuilder $formBuilder,Request $request,$id)
    {
        return [
            'forms' => [
                'form' => $formBuilder->create(FormCrudForm::class,[
                    'method' => 'PUT',
                    'url' => $this->crudUrlRoutes->update($id),
                    'model' => $this->formRepository->first($id)
                ])
            ],

        ];
    }

    public function update(Request $request,$id)
    {
        $this->formRepository->updateOrInsert($request,$id);
    }

    public function destroy($id)
    {
        $this->formRepository->delete($id);
    }

}
