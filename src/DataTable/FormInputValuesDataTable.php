<?php


namespace Azizyus\FormBuilder\DataTable;


use Azizyus\FormBuilder\ExternalStructureDefinitions\IIndexDataTableActionButtons;
use Azizyus\FormBuilder\Repositories\FormInputRepository;
use Azizyus\FormBuilder\Repositories\FormRepository;
use Azizyus\FormBuilder\Repositories\FormValueRepository;
use Yajra\DataTables\Services\DataTable;

class FormInputValuesDataTable extends DataTable
{

    /**
     * @var FormRepository
     */
    protected $repository;

    /**
     * @var IIndexDataTableActionButtons
     */
    protected $actionButtons;


    protected $inputId;

    public function __construct()
    {
        $this->repository = new FormValueRepository();
    }

    public function setActionButtons(IIndexDataTableActionButtons $c)
    {
        $this->actionButtons = $c;
    }

    public function setInputId(Int $id)
    {
        return $this->inputId = $id;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', 'forminputvaluesdatatable.action');
    }

    public function ajax()
    {
        return $this->dataTable($this->query())
            ->editColumn('isDefault',function($model){
                if($model->isDefault) return 'Yes';
                return 'No';
            })
            ->addColumn('action',$this->actionButtons->build())
            ->rawColumns(['link','action'])
            ->make(true);
    }


    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->repository->baseQuery()->where('inputId',$this->inputId)
            ->orderBy('sort','ASC')
            ;
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge([
                "id"=> ["title"=>"ID"],
                "sort"=> ["title"=>"Sort"],
                "isDefault"=> ["title"=>"Default"],
                "value"=> ["title"=>"Value"],
            ]))
            ->minifiedAjax()
            ->addAction([
                "title"=>'Options',
                "width" => '100px'
            ])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'value',
            'sort',
            'isDefault',

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'FormInputValues_' . date('YmdHis');
    }


}
