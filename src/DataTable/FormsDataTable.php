<?php


namespace Azizyus\FormBuilder\DataTable;


use Azizyus\FormBuilder\ExternalStructureDefinitions\IIndexDataTableActionButtons;
use Azizyus\FormBuilder\Repositories\FormRepository;
use Yajra\DataTables\Services\DataTable;

class FormsDataTable extends DataTable
{

    /**
     * @var FormRepository
     */
    protected $repository;

    /**
     * @var IIndexDataTableActionButtons
     */
    protected $actionButtons;

    protected $inputRouteName;

    public function __construct()
    {
        $this->repository = new FormRepository();
    }

    public function setActionButtons(IIndexDataTableActionButtons $c)
    {
        $this->actionButtons = $c;
    }

    public function setInputRouteName(String $name)
    {
        $this->inputRouteName = $name;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', 'formbuilderdatatable.action');
    }

    public function ajax()
    {
        return $this->dataTable($this->query())
            ->addColumn('inputs',function($model){
                return "<a href='".route($this->inputRouteName,['formId'=>$model->id])."'>Fields</a>";
            })
            ->addColumn('action',$this->actionButtons->build())
            ->rawColumns(['link','action','inputs'])
            ->make(true);
    }


    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->repository->baseQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge([
                "id"=> ["title"=>"ID"],
                "key"=> ["title"=>"KEY"],
                "title"=> ["title"=>"Title"],
                "inputs"=> ["title"=>"inputs"],
            ]))
            ->minifiedAjax()
            ->addAction([
                "title"=>'Options',
                "width" => '100px'
            ])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'title',

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'FormBuilder_' . date('YmdHis');
    }


}
