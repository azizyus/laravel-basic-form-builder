<?php


namespace Azizyus\FormBuilder\DataTable;


use Azizyus\FormBuilder\ExternalStructureDefinitions\IIndexDataTableActionButtons;
use Azizyus\FormBuilder\Repositories\FormInputRepository;
use Azizyus\FormBuilder\Repositories\FormRepository;
use Kris\LaravelFormBuilder\Field;
use Yajra\DataTables\Services\DataTable;

class FormInputsDataTable extends DataTable
{

    /**
     * @var FormRepository
     */
    protected $repository;

    /**
     * @var IIndexDataTableActionButtons
     */
    protected $actionButtons;


    protected $formId;

    protected $valuesRouteName;

    public function __construct()
    {
        $this->repository = new FormInputRepository();
    }

    public function setValuesRouteName($v)
    {
        $this->valuesRouteName = $v;
    }

    public function setActionButtons(IIndexDataTableActionButtons $c)
    {
        $this->actionButtons = $c;
    }

    public function setFormId(Int $id)
    {
        return $this->formId = $id;
    }

    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        return datatables($query)
            ->addColumn('action', 'formbuilderdatatable.action');
    }

    public function ajax()
    {
        return $this->dataTable($this->query())
            ->addColumn('values',function($model){

                if(in_array($model->type,[
                    Field::SELECT,
                    Field::CHECKBOX,
                    Field::RADIO,
                    Field::CHOICE
                ]))
                    return "<a href='".route( $this->valuesRouteName,['inputId'=>$model->id])."'>Values</a>";
                return "not multi value input";
            })
            ->addColumn('action',$this->actionButtons->build())
            ->rawColumns(['link','action','values'])
            ->make(true);
    }


    /**
     * Get query source of dataTable.
     *
     * @param \App\User $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query()
    {
        return $this->repository->baseQuery()->where('formId',$this->formId)->orderBy('sort','ASC');
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns(array_merge([
                "id"=> ["title"=>"ID"],
                "sort"=> ["title"=>"Sort"],
                "name"=> ["title"=>"Title"],
                "values"=> ["title"=>"Values"],
            ]))
            ->minifiedAjax()
            ->addAction([
                "title"=>'Options',
                "width" => '100px'
            ])
            ->parameters($this->getBuilderParameters());
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'id',
            'label',

        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'FormBuilder_' . date('YmdHis');
    }


}
