# DOC

This package provides basic crud structure of;
<pre>
form 
    form input
            form value(s)
</pre>
            
Every form parameter will be stored in DB, that means forms can be created by users and shared between views.                     
            
            
            
## Example Usage Scenario

- Add your form
- Define your inputs which you may reach from forms datatables.
- There will be many parameters your may provide like; name, label, isRequired, value and most importantly input class and wrapper class.
- If your input is one of the multiple valued type than you should provide those values from same datatable where you added your inputs.
- Your built forms should be listed in your page's crud page so you can pass them to page.

### View Helpers

<pre>
renderFormWithKey($forms,'formKeyWhichWasProvidedWhileCreatingForm');


@foreach($forms as $form)
    {!! renderSingleForm($form) !!}
@endforeach
</pre>




# INSTALLATION NOTES

## Register ServiceProviders

<pre>
 \Kris\LaravelFormBuilder\FormBuilderServiceProvider::class,
 \Azizyus\FormBuilder\FormBuilderServiceProvider::class,
</pre>




## NECESSARY CONFIG
<pre>
    \config()->set('formBuilder.crudViewName',panelViewPath('crud.edit'));
    \config()->set('formBuilder.dataTableViewName',panelViewPath('datatable.general-index'));
    \config()->set('payment-config.paymentMailView',themeViewPath('secure-payment-success-mail'));
</pre>


## TODO:
- Drag drop form builder 
- Form input inheritance
- CRUD page ui organisation (everything is line by line, related inputs might be next to each other)
